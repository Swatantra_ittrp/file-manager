import 'package:flutter/material.dart';

import 'package:slide_to_confirm/slide_to_confirm.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff556270),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {},
        label: Text("Boost"),
        icon: Icon(Icons.near_me),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MyAppBar(),
            const SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "File",
                    style: TextStyle(fontWeight: FontWeight.w800, fontSize: 30),
                  ),
                  IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.tune,
                        size: 30,
                        color: Colors.black,
                      ))
                ],
              ),
            ),
            const Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 18,
                ),
                child: Text(
                  "Manager",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey),
                ),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            const Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 18,
                ),
                child: Text(
                  "Let's clean and manage your files",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      color: Colors.black),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              // margin: EdgeInsets.only(left: 20),
              height: 230,
              child: Expanded(
                  child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  MyCards(
                      mycolor: Color(0xff22293d), myColor2: Color(0xff434e6c)),
                  MyCards(
                    mycolor: Color(0xff3787eb),
                    myColor2: Color(0xff1b70da),
                  ),
                  MyCards(
                      mycolor: Color(0xffff5a00), myColor2: Color(0xffc84e0c))
                ],
              )),
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Categories",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.black),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 75,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xffEAEAEA),
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: 10,
                      right: 18,
                    ),
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                        child: Icon(
                      Icons.music_note,
                      size: 35,
                      color: Color(0xffc471ed),
                    )),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Music",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Color(0xff576280)),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "223 items",
                        style: TextStyle(color: Color(0xff576280)),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 75,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xffEAEAEA),
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: 10,
                      right: 18,
                    ),
                    height: 60,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                        child: Icon(
                      Icons.video_camera_back,
                      size: 35,
                      color: Color(0xffff5a00),
                    )),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Videos",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: Color(0xff576280)),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "147 items",
                        style: TextStyle(color: Color(0xff576280)),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyCards extends StatelessWidget {
  Color mycolor;
  Color myColor2;

  MyCards({required this.mycolor, required this.myColor2});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      width: 180,
      decoration: BoxDecoration(
          color: mycolor, borderRadius: BorderRadius.circular(18)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Manager",
                style: TextStyle(
                    color: myColor2, fontSize: 16, fontWeight: FontWeight.w600),
              ),
              Icon(
                Icons.more_horiz,
                color: Colors.white,
              )
            ],
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Large",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                "92",
                style: TextStyle(
                    color: Color(0xffcfff00),
                    fontSize: 23,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "files",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                "Items",
                style: TextStyle(
                    color: myColor2, fontSize: 16, fontWeight: FontWeight.w600),
              ),
            ],
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            height: 8,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: LinearProgressIndicator(
                backgroundColor: myColor2,
                color: Color(0xffcfff00),
                value: 0.72,
              ),
            ),
          ),
          SizedBox(
            height: 3,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "72.40 GB / 128 GB",
              style: TextStyle(color: Colors.white, fontSize: 9),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            // height: 30,
            child: ConfirmationSlider(
              height: 30,
              onConfirmation: () {},
              backgroundColor: myColor2,
              text: "Swipe to clean >>>",
              textStyle: TextStyle(fontSize: 12),
              iconColor: Color(0xffcfff00),
              sliderButtonContent: Icon(
                Icons.arrow_forward_ios,
                size: 15,
              ),
            ),
          )
        ],
      ),
    );
  }

  confirmed() {}
}

class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(top: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.arrow_left,
                  size: 40,
                )),
            Container(
              margin: EdgeInsets.only(right: 18),
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(8),
              ),
              //
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.asset("assets/ironman2.png"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
